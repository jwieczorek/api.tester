﻿using System;

namespace API.Entities.Security
{
    public class XToken
    {
        public String Value { get; set; }
    }
}