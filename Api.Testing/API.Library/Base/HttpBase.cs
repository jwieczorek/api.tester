﻿using API.Entities;
using API.Entities.Security;

namespace API.Library.Base
{
    public class HttpBase
    {
        protected Helpers _helpers { get; set; } 
        protected XToken _xToken { get; set; }

        public HttpBase(WebFilter filter)
        {
            _xToken = filter.xToken;
            _helpers = filter.helpers;
        }
    }
}