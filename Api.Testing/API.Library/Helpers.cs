﻿using System;
using System.Text;

namespace API.Library
{
    public class Helpers
    {
        private System.Random _random { get; set; }
        
        public Helpers()
        {
            _random = new Random();
        }

        public Helpers(System.Random random)
        {
            _random = random;
        }
        
        public String RandomString(Int32 length=8)
        {
            String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJLKMNOPQURSTUVWXYZ1234567890";
            StringBuilder randomString = new StringBuilder();
            Int32 i;

            for (i = 0; i < length; ++i)
            {
                Char c = chars[_random.Next(0, chars.Length)];
                randomString.Append(c);
            }
            
            return randomString.ToString();
        }
        
    }
}