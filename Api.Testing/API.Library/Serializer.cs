﻿using System;
using Newtonsoft.Json;

namespace API.Library
{
    public class Serializer
    {

        public T DeSerialize<T>(String content)
        {
            return JsonConvert.DeserializeObject<T>(content);
        }
        
        public String Serialize<T>(T content)
        {
            return JsonConvert.SerializeObject(content);
        }
        
    }
}