﻿using System;
using API.Entities.Security;
using API.Library.WebMethods;

namespace API.Library
{
    public class WebAccessor
    {
        private System.Random _random { get; set; }
        private Helpers _helpers { get; set; }
        private WebFilter _filter { get; set; }
        public HttpGet Get { get; set; }
        public HttpPost Post { get; set; }
        public HttpPut Put { get; set; }
        public HttpDelete Delete { get; set; }
        
        public WebAccessor()
        {
            _random = new Random();
            
            WebFilter filter = new WebFilter()
            {
                xToken = new XToken(),
                helpers = new Helpers(_random) 
            };
            
            _filter = filter;
            
            Get = new HttpGet(ref filter);
            Post = new HttpPost(ref filter);
            Put = new HttpPut(ref filter);
            Delete = new HttpDelete(ref filter);
        }
        
    }
}