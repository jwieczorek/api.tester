﻿using System;
using API.Entities.Security;

namespace API.Library
{
    public class WebFilter
    {
        public XToken xToken { get; set; }
        public Helpers helpers { get; set; }
        public String apiBaseUrl { get; set; }
    }
}