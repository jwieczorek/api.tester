﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using API.Entities.Security;

namespace API.Library.WebMethods
{
    public class Http
    {
        private WebFilter _filter { get; set; }
        private HttpClient _client { get; set; }
        
        public Http(ref WebFilter filter)
        {
            _filter = filter;
            _client.BaseAddress = new Uri(_filter.apiBaseUrl);
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _client.DefaultRequestHeaders.Add("X-Token", _filter.xToken.Value);
        }
        
        public HttpResponseMessage Get(String uri, Dictionary<String, String> headers = null)
        {
            return _client.GetAsync(uri).Result;
        }

        public HttpResponseMessage Post<T>(String uri, T data)
        {
            return _client.PostAsJsonAsync(uri, data).Result;
        }
        
    }
}