﻿using System;
using System.Collections.Generic;;

namespace API.Library.WebMethods
{
    public class HttpGet : Base.HttpBase
    {
        private Http _http { get; set; }
        
        public HttpGet(ref WebFilter filter) : base(filter)
        {
            _http = new Http(ref filter);
        }
        

        #region "Get Office"
        public object Office(Int32 count = 0)
        {
            List<object> offices = new List<object>();
            
            for (Int32 i; i < count; i++)
            {
                offices.Add(Office());
            }

            return offices;
        }
        
        public object Office()
        {
            return _http.Get("Office").Content.ReadAsStreamAsync().Result;
        }
        #endregion
       
    }
}